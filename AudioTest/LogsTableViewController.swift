//
//  LogsTableViewController.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import UIKit
import Cadmus
import Owl

class LogsTableViewController: UITableViewController {

	lazy var director: TableDirector = {
		return TableDirector(table: tableView)
	}()
	
	var mainSection: TableSection!

	override func viewDidLoad() {
		super.viewDidLoad()
		
		mainSection = TableSection(id: "logs", elements: [])
		director.rowHeight = .auto(estimated: 44)
		
		director.add(section: mainSection)
		
		registerAdapters()

		LogManager.shared.onLog = logged
	}
	
	func registerAdapters() {
		registerLogAdapter()
	}
	
	func registerLogAdapter() {
		let adapter = TableCellAdapter<LogItem, LogTableViewCell>()
		adapter.events.dequeue = { ctx in
			ctx.cell?.item = ctx.element
		}
		adapter.events.didSelect = { ctx in
			return .deselect
		}
		
		director.registerCellAdapter(adapter)
	}
	
	func logged(_ message: String) {
		guard Thread.isMainThread else {
			DispatchQueue.main.async {
				self.logged(message)
			}
			return
		}
		mainSection.add(element: LogItem(value: message), at: 0)
		director.reload()
	}

}

class LogManager: CadmusDelegate {
	
	static let shared: LogManager = LogManager()
	
	var onLog: ((String) -> Void)?
	
	func log(prefix: String, message: String, file: StaticString, function: StaticString, line: UInt) {
		let components = file.description.components(separatedBy: "/")
		var log = message
		if let name = components.last {
			log = "[\(prefix) \(name):\(function)@\(line)]: \(message)"
		} else {
			log = "[\(prefix) \(file):\(function)@\(line)]: \(message)"
		}
		print(log)
		onLog?(log)
	}
	
	public func log(verbose: String, file: StaticString, function: StaticString, line: UInt) {
		log(prefix: "📢", message: verbose, file: file, function: function, line: line)
	}
	
	public func log(verbose: CustomStringConvertible, file: StaticString, function: StaticString, line: UInt) {
		log(verbose: verbose.description, file: file, function: function, line: line)
	}
	
	public func log(info: String, file: StaticString, function: StaticString, line: UInt) {
		log(prefix: "💡", message: info, file: file, function: function, line: line)
	}
	
	public func log(info: CustomStringConvertible, file: StaticString, function: StaticString, line: UInt) {
		log(info: info.description, file: file, function: function, line: line)
	}
	
	public func log(warning: String, file: StaticString, function: StaticString, line: UInt) {
		log(prefix: "☢", message: warning, file: file, function: function, line: line)
	}
	
	public func log(warning: CustomStringConvertible, file: StaticString, function: StaticString, line: UInt) {
		log(warning: warning.description, file: file, function: function, line: line)
	}
	
	public func log(warning: Error, file: StaticString, function: StaticString, line: UInt) {
		log(warning: "\(warning)", file: file, function: function, line: line)
	}
	
	public func log(error: String, file: StaticString, function: StaticString, line: UInt) {
		log(prefix: "🔥", message: error, file: file, function: function, line: line)
	}
	
	public func log(error: CustomStringConvertible, file: StaticString, function: StaticString, line: UInt) {
		log(error: error.description, file: file, function: function, line: line)
	}
	
	public func log(error: Error, file: StaticString, function: StaticString, line: UInt) {
		log(error: "\(error)", file: file, function: function, line: line)
	}
	
	public func log(debug: String, file: StaticString, function: StaticString, line: UInt) {
		log(prefix: "🐞", message: debug, file: file, function: function, line: line)
	}
	
	public func log(debug: CustomStringConvertible, file: StaticString, function: StaticString, line: UInt) {
		log(debug: debug.description, file: file, function: function, line: line)
	}
}
