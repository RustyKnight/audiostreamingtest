//
//  InputTableViewCell.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import UIKit
import Owl
import SkyFloatingLabelTextField

class InputItem: ElementRepresentable {
	var differenceIdentifier: String = UUID().uuidString
	
	let placeHolder: String?
	var value: String?
	
	init(placeHolder: String, value: String? = nil) {
		self.placeHolder = placeHolder
		self.value = value
	}
	
	func isContentEqual(to other: Differentiable) -> Bool {
		guard let other = other as? InputItem else { return false }
		return other.value == value
	}
}

class InputTableViewCell: UITableViewCell {
	
	@IBOutlet weak var inputField: SkyFloatingLabelTextField!
	
	var item: InputItem? {
		didSet {
			inputField.text = item?.value
			inputField.placeholder = item?.placeHolder
		}
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	@IBAction func editingDidEnd(_ sender: Any) {
		item?.value = inputField.text
	}
}
