//
//  InputTableViewCell.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import UIKit
import Owl

class LogItem: ElementRepresentable {
	var differenceIdentifier: String = UUID().uuidString
	
	var value: String
	
	init(value: String) {
		self.value = value
	}
	
	func isContentEqual(to other: Differentiable) -> Bool {
		guard let other = other as? InputItem else { return false }
		return other.value == value
	}
}

class LogTableViewCell: UITableViewCell {
	
	@IBOutlet weak var logLabel: UILabel!
	
	var item: LogItem? {
		didSet {
			logLabel.text = item?.value
		}
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
}
