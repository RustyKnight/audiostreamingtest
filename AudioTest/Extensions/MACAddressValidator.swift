//
//  MacAddressValidator.swift
//  SwannSecurity
//
//  Created by Shane Whitehead on 11/6/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

// Yes, this could be String extension, but really, how many of these would
// I end up with and would it make it eaiser/cleaner?
struct MACAddressValidator {
	
	// bc51fe400466
	static let shortPattern = "^[a-fA-F0-9]{12}$"
	// Match BC:51:fe:40:04:66 or BC-51-fe-40-04-66
	static let longPattern = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$"

	static func isValidShort(macAddress: String) -> Bool {
		//		guard length ~= password.count else { return false }
		return macAddress ~= shortPattern
	}

	static func isValidLong(macAddress: String) -> Bool {
		//		guard length ~= password.count else { return false }
		return macAddress ~= longPattern
	}

}

extension NSRegularExpression {
	convenience init(_ pattern: String) {
		do {
			try self.init(pattern: pattern)
		} catch {
			preconditionFailure("Illegal regular expression: \(pattern).")
		}
	}
}

extension NSRegularExpression {
	func matches(_ string: String) -> Bool {
		let range = NSRange(location: 0, length: string.utf16.count)
		guard let match = firstMatch(in: string, options: [], range: range) else { return false }
		return match.numberOfRanges == 1 && match.range == range
	}
}

extension String {
	static func ~= (lhs: String, rhs: String) -> Bool {
		guard let regex = try? NSRegularExpression(pattern: rhs) else { return false }
		let range = NSRange(location: 0, length: lhs.utf16.count)
		return regex.firstMatch(in: lhs, options: [], range: range) != nil
	}
}

