//
//  String+Substring.swift
//  SwannSecurity
//
//  Created by Shane Whitehead on 5/7/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

extension String {

	func substring(from: Int, exclusiveTo to: Int) -> String {
		return substring(of: from..<to)
	}

	func substring(from: Int, inclusiveTo to: Int) -> String {
		return substring(of: from...to)
	}

	func substring(of range: ClosedRange<Int>) -> String {
		let startAt = index(startIndex, offsetBy: range.lowerBound)
		let endAt = index(startIndex, offsetBy: range.upperBound)
		let part = self[startAt...endAt]
		return String(part)
	}
	
	func substring(of range: Range<Int>) -> String {
		let startAt = index(startIndex, offsetBy: range.lowerBound)
		let endAt = index(startIndex, offsetBy: range.upperBound)
		let part = self[startAt..<endAt]
		return String(part)
	}
}
