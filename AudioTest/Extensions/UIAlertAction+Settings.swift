//
//  UIAlertAction+Settings.swift
//  SwannOne
//
//  Created by Shane Whitehead on 31/1/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import UIKit

enum SettingAction {
	case success
	case canNotOpenSettings
	case requestFailed
}

extension UIAlertAction {
	
	static func settings(style: UIAlertAction.Style = .default, then: @escaping (SettingAction) -> Void) -> UIAlertAction{
		let settingsAction = UIAlertAction(title: "Change permission", style: .default) { (action) in
			guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
				then(.requestFailed)
				return
			}
			guard UIApplication.shared.canOpenURL(settingsUrl) else {
				then(.canNotOpenSettings)
				return
			}
			
			UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
				guard !success else {
					then(.success)
					return
				}
				then(.requestFailed)
			})
		}
		
		return settingsAction
	}

}
