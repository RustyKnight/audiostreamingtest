//
//  InputTableViewController.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import UIKit
import Owl
import Cadmus

// The ports are a reflection of what a client might need,
// so the "writeToPort" is the port that the client should write
// to, but is the port that the app is reading from ... 🤪
struct DeviceConfiguration: Codable {
	let ipAddress: String
	let port: UInt16
}

class InputTableViewController: UITableViewController {

	lazy var director: TableDirector = {
		return TableDirector(table: tableView)
	}()

	var deviceConfiguration: DeviceConfiguration {
		return DeviceConfiguration(ipAddress: ipAddressItem.value ?? "",
															 port: UInt16(portItem.value ?? "0") ?? 0)
	}

	let ipAddressItem: InputItem = InputItem(placeHolder: "IP address")
	let portItem: InputItem = InputItem(placeHolder: "Port been written to")

	var deviceSettingsSection: TableSection!

	override func viewDidLoad() {
		super.viewDidLoad()
		
		director.rowHeight = .auto(estimated: 44)
		
		registerAdapters()
		
		ipAddressItem.value = "192.168.202.9"
		portItem.value = "8686"

		deviceSettingsSection = TableSection(id: "DeviceSetings", elements: [
			ipAddressItem,
			portItem,
			], header: "Device Settings")

		director.add(section: deviceSettingsSection)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		NotificationCenter.default.addObserver(self, selector: #selector(configurationLoaded(_:)), name: ConfigImporter.serverConfigurationRecieved, object: nil)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		dismissKeyboardOnTap = true
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		dismissKeyboardOnTap = false
	}
	
	func registerAdapters() {
		registerInputAdapter()
	}
	
	func registerInputAdapter() {
		let adapter = TableCellAdapter<InputItem, InputTableViewCell>()
		adapter.events.dequeue = { ctx in
			ctx.cell?.item = ctx.element
		}
		adapter.events.didSelect = { ctx in
			return .deselect
		}
		
		director.registerCellAdapter(adapter)
	}
	
	@objc func configurationLoaded(_ notification: Notification) {
		guard let config = notification.object as? DeviceConfiguration else {
			log(warning: "Configuration loaded without supplying appropriate payload")
			return
		}
		
		log(debug: "config.remotePort = \(config.port)")
		
		director.reload()
	}
}
