//
//  MicrophoneService.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

	import Foundation
	import AVFoundation
	import Cadmus

	// Base on https://stackoverflow.com/questions/39595444/avaudioengine-downsample-issue
	// https://github.com/onmyway133/notes/issues/367

	class MicrophoneService {
		
		let audioEngine = AVAudioEngine()
		
	//	let inputFormat: AVAudioFormat
		
		init() {
			do {
				try AVAudioSession.sharedInstance().setPreferredSampleRate(16000)
			} catch let error {
				log(error: "\(error)")
			}
			
			let engineInputNode = audioEngine.inputNode
			let bus = 0
			let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
			
			//engineInputNode.installTap(onBus: bus, bufferSize: 1024, format: engineInputNodeFormat) { (buffer, time) in
			//	AudioEncoderQueue.shared.put(buffer)
			//}
			
			let mixer = AVAudioMixerNode()
			audioEngine.attach(mixer)

			let mixerOutputFormat = AVAudioFormat(standardFormatWithSampleRate: 8000, channels: 1)

			audioEngine.connect(engineInputNode, to: mixer, format: engineInputNodeFormat)
			audioEngine.connect(mixer, to: audioEngine.outputNode, format: mixerOutputFormat)

			mixer.installTap(onBus: bus, bufferSize: 1024 * 4, format: mixerOutputFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
				AudioEncoderQueue.shared.put(buffer)
			}
		}
		
		func start() throws {
			stop()
			log(debug: ">> Start")
			audioEngine.prepare()
			try audioEngine.start()
		}
		
		func stop() {
			audioEngine.stop()
			log(debug: "<< Stop")
		}
		
	}
