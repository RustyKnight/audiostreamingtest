//
//  Validator.swift
//  SwannOne
//
//  Created by Shane Whitehead on 16/1/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

// So it can be tested
struct Validator {
	
	static func isValid(email: String) -> Bool {
		let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
			"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
			"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
			"z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
			"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
			"9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
		"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
		let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
		return emailPredicate.evaluate(with: email)
	}
	
	static func isValid(ip: String) -> Bool {
		let parts = ip.components(separatedBy: ".")
		let nums = parts.compactMap { Int($0) }
		return parts.count == 4 && nums.count == 4 && !nums.contains { $0 < 0 || $0 > 255 }
			//nums.filter { $0 >= 0 && $0 < 256}.count == 4
	}
	
	static func isValid(deviceIdentifier: String) -> Bool {
		let pattern = "[0-9A-Z]{20}+"
		return deviceIdentifier ~= pattern
	}

}
