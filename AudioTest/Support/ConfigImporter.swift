//
//  ConfigImporter.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import Cadmus

struct ConfigImporter {
	
	static let serverConfigurationRecieved = Notification.Name(rawValue: "serverConfigurationRecieved")
	
	static func importData(from url: URL) {
		do {
			let data = try Data(contentsOf: url)
			let config = try JSONDecoder().decode(DeviceConfiguration.self, from: data)
			
			log(info: "Imported remote configuration")
			NotificationCenter.default.post(name: serverConfigurationRecieved, object: config)
		} catch let error {
			log(error: "\(error)")
		}
	}
	
}
