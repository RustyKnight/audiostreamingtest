//
//  AudioEncoderService.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

	import Foundation
	import AVFoundation
	import Cadmus

	class AudioEncoderService {
		
		fileprivate var stopped: Bool = false
		
		fileprivate let audioFile: AVAudioFile

		init() throws {

			var url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
			url.appendPathComponent("Test.aac", isDirectory: false)
			
			let settings: [String: Any] = [
				AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
				AVSampleRateKey: NSNumber(value: 8000),
				AVNumberOfChannelsKey: NSNumber(value: 1),
				AVEncoderBitRatePerChannelKey: NSNumber(value: 16),
				AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.high.rawValue)
			]
			audioFile = try AVAudioFile(forWriting: url, settings: settings)
		}
		
		var audioEngine: AVAudioEngine?
		var playerNode: AVAudioPlayerNode?
		
		func start() {
			
			let audioEngine = AVAudioEngine()
			let playerNode = AVAudioPlayerNode()

			self.audioEngine = audioEngine
			self.playerNode = playerNode
			
			let format = AVAudioFormat(standardFormatWithSampleRate: 8000, channels: 1)
			
			audioEngine.attach(playerNode)
			audioEngine.connect(playerNode, to: audioEngine.mainMixerNode, format: format)

			DispatchQueue.global(qos: .userInitiated).async {
				
				log(debug: "Prepaer player")
				audioEngine.prepare()
				do {
					log(debug: "Start player")
					try audioEngine.start()
				} catch let error {
					log(error: "Failed to initialise player \(error.localizedDescription)")
				}
				
				playerNode.play()
				
				log(debug: "Start encoder service")
				self.encodeAudio()
			}
		}
		
		func stop() {
			stopped = true
		}
		
		func encodeAudio() {
			repeat {
				do {
					if let buffer = AudioEncoderQueue.shared.take() {
						guard let encodedBuffer = try encode(buffer) else { continue }
						
						if let decoded = try AudioUtilities.convertToPCM(from: encodedBuffer) {
							playerNode?.scheduleBuffer(decoded, completionHandler: {
								log(debug: "Played")
							})
						} else {
							log(debug: "Failed to decode buffer")
						}
	//					do {
	//						try audioFile.write(from: buffer)
	//					} catch let error {
	//						log(error: "\(error)")
	//					}

						
						let data = Data(bytes: encodedBuffer.data, count: Int(encodedBuffer.byteLength))
						log(debug: "Data = \(data.count)")
						TransportQueue.shared.put(data)
					}
				} catch let error {
					log(error: "Failed to resample/encode audio buffer: \(error.localizedDescription)")
				}
			} while !stopped
			log(debug: "Stopped")
			
			audioEngine?.stop()
			audioEngine = nil
			playerNode?.stop()
			playerNode = nil
		}
		
		func encode(_ buffer: AVAudioPCMBuffer) throws -> AVAudioCompressedBuffer? {
			return try AudioUtilities.convertToAAC(from: buffer)
		}
		
		func toData(buffer: AVAudioPCMBuffer) -> Data {
			let audioBuffer = buffer.audioBufferList.pointee.mBuffers
			return Data(bytes: audioBuffer.mData!, count: Int(audioBuffer.mDataByteSize))
		}
		
	//	func resample(_ buffer: AVAudioPCMBuffer) throws -> AVAudioPCMBuffer? {
	//		guard let pcmBuffer = AVAudioPCMBuffer(pcmFormat: resampleFormat, frameCapacity: AVAudioFrameCount(resampleFormat.sampleRate * 2.0)) else { return nil }
	//
	//		let inputBlock: AVAudioConverterInputBlock = { inNumPackets, outStatus in
	//			outStatus.pointee = AVAudioConverterInputStatus.haveData
	//			return buffer
	//		}
	//
	//		var error: NSError? = nil
	//		resampleConverter.convert(to: pcmBuffer, error: &error, withInputFrom: inputBlock)
	//
	//		guard let resampleError = error else { return pcmBuffer }
	//
	//		throw resampleError
	//	}
		
	}
