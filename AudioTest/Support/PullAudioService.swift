//
//  OutgoingAudioServer.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//
//
//import Foundation
//import SwiftSocket
//import Cadmus
//import AVFoundation
//
//class PullAudioService {
//	
//	let config: DeviceConfiguration
//	
//	var socket: TCPClient?
//	
//	init(config: DeviceConfiguration) {
//		self.config = config
//	}
//	
//	func open() {
//		guard socket == nil else { return }
//		
//		log(debug: "Create socket...\(config.ipAddress):\(config.localPort)")
//		socket = TCPClient(address: config.ipAddress, port: Int32(config.localPort))
//		switch socket!.connect(timeout: 30) {
//		case .success:
//			readData()
//		case .failure(let error):
//			log(error: "\(error)")
//		}
//	}
//	
//	func close() {
//		log(debug: "Close socket")
//		socket?.close()
//		socket = nil
//	}
//	
//	fileprivate func readData() {
//		guard let socket = socket else {
//			log(warning: "Socket is not open")
//			return
//		}
//		initPlayer()
//		defer {
//			disposePlayer()
//			close()
//		}
//		let headerLength = MemoryLayout<SWANN_AUDIO_HEADER_ST>.size
//		var lastSequence: Int32 = -1
//		repeat {
//			guard let bytes = socket.read(headerLength, timeout: 5) else {
//				log(warning: "Did not read any data for header")
//				return
//			}
//			
//			let ump = UnsafeMutablePointer(mutating: bytes)
//			let umrp = UnsafeMutableRawPointer(mutating: ump)
//			let amb = umrp.assumingMemoryBound(to: SWANN_AUDIO_HEADER_ST.self)
//			
//			let header = amb.pointee
//			
//			guard header.packet_sn == (lastSequence + 1) else {
//				log(warning: "Out of sequence order, expcting \(lastSequence + 1) got \(header.packet_sn)")
//				return
//			}
//			
//			lastSequence = header.packet_sn
//			
//			log(debug: "Seq = \(header.packet_sn); \(header.data_length); \((Int(header.data_length) + headerLength))")
//			
//			guard let buffer = socket.read(Int(header.data_length), timeout: 5) else {
//				log(warning: "Did not read any data for buffer")
//				return
//			}
//			
//			play(buffer)
//		} while true
//	}
//	
//	var audioEngine: AVAudioEngine?
//	var audioPlayer: AVAudioPlayerNode?
//	
//	func initPlayer() {
//		disposePlayer()
//		
//		guard let format = AudioUtilities.PCMFormat() else { return }
//		audioPlayer = AVAudioPlayerNode()
//		guard let audioPlayer = audioPlayer else { return }
//
//		audioEngine = AVAudioEngine()
//		guard let audioEngine = audioEngine else { return }
//		
//		audioEngine.attach(audioPlayer)
//		audioEngine.connect(audioPlayer, to: audioEngine.mainMixerNode, format: format)
//		audioEngine.prepare()
//		
//		do {
//			try audioEngine.start()
//		} catch let error {
//			log(error: "Failed to initialise audio engine: \(error.localizedDescription)")
//		}
//	}
//	
//	func disposePlayer() {
//		audioPlayer?.stop()
//		audioEngine?.stop()
//		audioEngine = nil
//		audioPlayer = nil
//	}
//	
//	func play(_ buffer: [Byte]) {
//		guard let player = audioPlayer else { return }
//		DispatchQueue.global().async {
//			do {
//				let data = Data(bytes: buffer, count: buffer.count)
//				
//				let description = AudioStreamPacketDescription(mStartOffset: 0,
//																											 mVariableFramesInPacket: 0,
//																											 mDataByteSize: UInt32(buffer.count))
//				guard let aacBuffer = AudioUtilities.convertToAAC(from: data, packetDescriptions: [description]) else {
//					log(warning: "Failed to decode data to AAC audio buffer")
//					return
//				}
//				guard let pcmBuffer = try AudioUtilities.convertToPCM(from: aacBuffer) else {
//					log(warning: "Failed to decode AAC audio buffer to PCM audio buffer")
//					return
//				}
//				player.scheduleBuffer(pcmBuffer, completionHandler: {
//					
//				})
//			} catch let error {
//				log(error: "Failed to decode and play frame: \(error.localizedDescription)")
//			}
//		}
//	}
//	
//}
