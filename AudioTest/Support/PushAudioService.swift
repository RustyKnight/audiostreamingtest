//
//  OutgoingAudioServer.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import SwiftSocket
import Cadmus

class PushAudioService {
	
	var transporter: TransportService?
	var encoder: AudioEncoderService?
	var microphone: MicrophoneService?
	
	func start(_ config: DeviceConfiguration) throws {
		stop()
		
		do {
			TransportQueue.shared.start()
			transporter = TransportService(config: config)
			try transporter?.start()
			
			microphone = MicrophoneService()
			
			AudioEncoderQueue.shared.start()
			encoder = try AudioEncoderService()
			encoder?.start()
			
			try microphone?.start()
		} catch let error {
			stop()
			throw error
		}
	}
	
	func stop() {
		microphone?.stop()
		
		AudioEncoderQueue.shared.stop()

		encoder?.stop()
		
		TransportQueue.shared.stop()
		
		transporter?.stop()
	}
	
}
//
//	let config: DeviceConfiguration
//
//	var socket: TCPServer?
//
//	init(config: DeviceConfiguration) {
//		self.config = config
//	}
//
//	func open() {
//		guard socket == nil else { return }
//
//		log(debug: "Create server socket on port \(config.port)")
//		socket = TCPServer(address: config.ipAddress, port: Int32(config.port))
//
//		guard let socket = socket else { return }
//
//		log(debug: "Listen for incoming connections")
//		switch socket.listen() {
//		case .success:
//			while true {
//				log(debug: "Wait for connection")
//				guard let client = socket.accept() else {
//					log(error: "Failed to accept incoming request")
//					continue
//				}
//				let pusher = AudioPusher(client)
//				pusher.pushAudio()
//			}
//		case .failure(let error):
//			log(error: "Failed to estabish AudioPush server: \(error)")
//		}
//
////		switch socket!.connect(timeout: 30) {
////		case .success: sendData {
////				self.close()
////			}
////		case .failure(let error):
////			log(error: "\(error)")
////		}
//	}
//
//	func close() {
//		log(debug: "Close socket")
//		socket?.close()
//		socket = nil
//	}
//
//}
//
//class AudioPusher {
//
//	let socket: TCPClient
//
//	init(_ socket: TCPClient) {
//		self.socket = socket
//	}
//
//	func pushAudio() {
//		DispatchQueue.global().async {
//			self.sendData()
//		}
//	}
//
//	fileprivate func sendData() {
//		defer {
//			log(info: "Closing client socket \(socket.address)")
//			socket.close()
//		}
//
//		guard let frameBuffer = try? FrameBuffer() else {
//			log(error: "Failed to load frame buffer")
//			return
//		}
//
//		let headerLength = MemoryLayout<SWANN_AUDIO_HEADER_ST>.size
//		var count = 0
//
//		log(debug: "headerLength = \(headerLength)")
//
//		while frameBuffer.hasNext() {
//			let nextFrame = frameBuffer.next()
//
//			var header = SWANN_AUDIO_HEADER_ST()
//			header.start_code = (Int8("S".utf8.first!), Int8("W".utf8.first!), Int8("S".utf8.first!), Int8("W".utf8.first!))
//			header.header_length = Int32(headerLength)
//			header.encoder_type = 2
//			header.sample_rate = 8000
//			header.packet_sn = Int32(count)
//			header.data_length = Int32(nextFrame.count)
//
//			//			log(debug: "header.start_code = \(header.start_code)")
//			//			log(debug: "header.header_length = \(header.header_length)")
//			//			log(debug: "header.encoder_type = \(header.encoder_type)")
//			//			log(debug: "header.sample_rate = \(header.sample_rate)")
//			//			log(debug: "header.packet_sn = \(header.packet_sn)")
//			//			log(debug: "header.data_length = \(header.data_length)")
//
//			var headerData = Data(bytes: &header, count: headerLength)
//			headerData.append(nextFrame)
//			log(debug: "Seq = \(count); \(nextFrame.count); \(headerData.count)")
//			let result = socket.send(data: headerData)
//			switch result {
//			case .success: break
//			case .failure(let error):
//				log(error: "\(error)")
//				return
//			}
//			count += 1
//			Thread.sleep(forTimeInterval: 0.1)
//		}
//		Thread.sleep(forTimeInterval: 1.0)
//	}
//
//}
