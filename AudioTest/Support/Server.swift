//
//  Server.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

//
//
//func makeConnection() {
//	guard let ipAddress = ipAddressTextField.text, Validator.isValid(ip: ipAddress) else {
//		presentErrorAlertWith(message: "Invalid IP Address")
//		return
//	}
//	UserDefaults.standard.set(ipAddress, forKey: "Camera.ipAddress")
//	guard let value = incomingPortTextField.text, let port = Int32(value), port > 0 && port <= 65535 else {
//		presentErrorAlertWith(message: "Invalid Port")
//		return
//	}
//	
//	listenButton.setTitle("Stop listening", for: [])
//	
//	let size = MemoryLayout<SWANN_AUDIO_HEADER_ST>.size
//	log(debug: "size = \(size)")
//	
//	log(debug: "Create socket...\(ipAddress):\(port)")
//	socket = UDPClient(address: ipAddress, port: port)
//	
//	DispatchQueue.global().async {
//		guard let socket = self.socket else { return }
//		log(debug: "Read \(size) bytes...")
//		
//		let response = socket.recv(size)
//		let bytes = response.0
//		let name = response.1
//		let size = response.2
//		
//		log(debug: "Read \(size) bytes from \(name)")
//	}
//}
//
//func closeConnection() {
//	listenButton.setTitle("Listen", for: [])
//	guard let socket = socket else { return	}
//	socket.close()
//	self.socket = nil
//}
//
//@IBAction func cyclePlay(_ sender: Any) {
//	if isPlaying {
//		stopPlaying()
//	} else {
//		startPlaying()
//	}
//}
//
//func startPlaying() {
//	guard let ipAddress = ipAddressTextField.text, Validator.isValid(ip: ipAddress) else {
//		presentErrorAlertWith(message: "Invalid IP Address")
//		return
//	}
//	UserDefaults.standard.set(ipAddress, forKey: "Camera.ipAddress")
//	guard let value = outgoingPortTextField.text, let port = Int32(value), port > 0 && port <= 65535 else {
//		presentErrorAlertWith(message: "Invalid Port")
//		return
//	}
//	
//	UserDefaults.standard.set(value, forKey: "Camera.outgoingPort")
//	
//	isPlaying = true
//	playButton.setTitle("Stop Playing", for: [])
//	
//	DispatchQueue.global().async {
//		do {
//			try self.sendData(to: ipAddress, port: port) {
//				self.stopPlaying()
//			}
//		} catch let error {
//			log(error: "\(error)")
//			self.stopPlaying()
//		}
//	}
//}
//
//func stopPlaying() {
//	DispatchQueue.main.async {
//		self.tearDownPlayer()
//		self.isPlaying = false
//		self.playButton.setTitle("Play", for: [])
//	}
//}
//
//@IBAction func cycleTunnelPlay(_ sender: Any) {
//	if isTunneling {
//		stopTunnelPlay()
//	} else {
//		startTunnelPlay()
//	}
//}
//
//func startTunnelPlay() {
//	guard let identifier = deviceIdentifierTextField.text else {
//		presentErrorAlertWith(message: "Invalid device identifier")
//		return
//	}
//	guard let macAddress = macAddressTextField.text else {
//		presentErrorAlertWith(message: "Inavlid mac address")
//		return
//	}
//	guard MACAddressValidator.isValidLong(macAddress: macAddress) || MACAddressValidator.isValidShort(macAddress: macAddress) else {
//		presentErrorAlertWith(message: "Inavlid mac address")
//		return
//	}
//	
//	log(debug: "long = \(long)")
//	log(debug: "short = \(short)")
//	
//	UserDefaults.standard.set(identifier, forKey: "Camera.deviceIdentifier")
//	UserDefaults.standard.set(macAddress, forKey: "Camera.macAddress")
//	
//	guard let value = outgoingPortTextField.text, let port = Int32(value), port > 0 && port <= 65535 else {
//		presentErrorAlertWith(message: "Invalid Port")
//		return
//	}
//	UserDefaults.standard.set(value, forKey: "Camera.outgoingPort")
//	
//	deviceIdentifier = identifier
//	self.macAddress = macAddress
//	
//	self.isTunneling = true
//	self.playOverTunnelButton.setTitle("Stop (over tunnel)", for: [])
//	
//	tunnelPlay(port: UInt16(port)) {
//		self.stopTunnelPlay()
//	}
//}
//
//func stopTunnelPlay() {
//	DispatchQueue.main.async {
//		self.tearDownPlayer()
//		self.isTunneling = false
//		self.playOverTunnelButton.setTitle("Play (over tunnel)", for: [])
//	}
//}
//
//
//func sendData(to ipAddress: String, port: Int32, then: () -> Void) throws {
//	guard let frameBuffer = try FrameBuffer() else {
//		then()
//		return
//	}
//	
//	log(debug: "Create socket...\(ipAddress):\(port)")
//	sendSocket = UDPClient(address: ipAddress, port: port)
//	guard let socket = sendSocket else {
//		then()
//		return
//	}
//	
//	defer {
//		socket.close()
//		then()
//	}
//	
//	let headerLength = MemoryLayout<SWANN_AUDIO_HEADER_ST>.size
//	var count = 0
//	
//	log(debug: "headerLength = \(headerLength)")
//	
//	while frameBuffer.hasNext() {
//		let nextFrame = frameBuffer.next()
//		
//		var header = SWANN_AUDIO_HEADER_ST()
//		header.start_code = (Int8("S".utf8.first!), Int8("W".utf8.first!), Int8("S".utf8.first!), Int8("W".utf8.first!))
//		header.header_length = Int32(headerLength)
//		header.encoder_type = 2
//		header.sample_rate = 8000
//		header.packet_sn = Int32(count)
//		header.data_length = Int32(nextFrame.count)
//		
//		log(debug: "header.start_code = \(header.start_code)")
//		log(debug: "header.header_length = \(header.header_length)")
//		log(debug: "header.encoder_type = \(header.encoder_type)")
//		log(debug: "header.sample_rate = \(header.sample_rate)")
//		log(debug: "header.packet_sn = \(header.packet_sn)")
//		log(debug: "header.data_length = \(header.data_length)")
//		
//		var headerData = Data(bytes: &header, count: headerLength)
//		headerData.append(nextFrame)
//		log(debug: "Seq = \(count); \(nextFrame.count); \(headerData.count)")
//		let result = socket.send(data: headerData)
//		switch result {
//		case .success: break
//		case .failure(let error):
//			log(error: "\(error)")
//			return
//		}
//		count += 1
//		Thread.sleep(forTimeInterval: 0.1)
//	}
//}
//
//func tearDownPlayer() {
//	sendSocket?.close()
//	
//	guard let tunnel = tunnel else { return }
//	tunnel.close().always(in: .main) {
//		self.tunnel = nil
//		}.catch(in: .main) { (error) in
//			log(error: "\(error)")
//	}
//}
//
//func readFile() throws -> Data? {
//	guard let url = Bundle.main.url(forResource: "sample", withExtension: "aac") else { return nil }
//	return try Data(contentsOf: url)
//}
//
//var macAddress: String = "bc51fefffffa"
//var deviceIdentifier: String = "CPUTU8DVYNC1MGPX111A"
//var tunnel: Tunnel?
//
//internal var tunnelUserName: String? {
//	return macAddress.replacingOccurrences(of: ":", with: "").replacingOccurrences(of: "-", with: "").lowercased()
//}
//
//internal var tunnelPassword: String? {
//	guard let mac = tunnelUserName else { return nil }
//	
//	let part1 = mac.substring(of: 4..<12)
//	let part2 = deviceIdentifier.substring(of: 4..<8)
//	
//	let code = part1 + part2
//	let base64 = code.data(using: .utf8)!.base64EncodedString()
//	return base64
//}
//
//func tunnelPlay(port: UInt16, then: @escaping () -> Void) {
//	log(debug: "Connect to tunnel...")
//	Promise<Void>(in: .userInitiated) { (fulfill, fail, _) in
//		fulfill(())
//		}.then(in: .userInitiated) { () -> Promise<Tunnel> in
//			return TunnelFactory.shared.make(deviceIdentifier: self.deviceIdentifier,
//																			 withUsername: self.tunnelUserName,
//																			 andPassword: self.tunnelPassword)
//		}.then(in: .main) { (tunnel) -> Promise<UInt16> in
//			self.tunnel = tunnel
//			log(debug: "Map \(port) port...")
//			return self.map(remotePort: port, through: tunnel)
//		}.then(in: .userInitiated) { (mappedPort) in
//			try self.sendData(to: "127.0.0.1", port: Int32(mappedPort), then: then)
//		}.catch(in: .main) { (error) in
//			log(error: "\(error)")
//			self.presentErrorAlertWith(message: "\(error)")
//			self.stopTunnelPlay()
//	}
//}
//
//func map(remotePort: UInt16, through tunnel: Tunnel) -> Promise<UInt16> {
//	return tunnel.map(remotePort: remotePort).then({ (results) in
//		// Get the first result which doesn't have an error...
//		guard let mapping = (results.first { $0.error == nil }) else {
//			if let failedMapping = (results.first { $0.error != nil }) {
//				log(error: "Failed to map remote port \(failedMapping.error ?? .unknown)")
//			}
//			throw Error.portMappingFailed
//		}
//		guard let port = mapping.localPort else {
//			log(error: "Port mapping did not generate error, but did not return a local port")
//			throw Error.portMappingFailed
//		}
//		return Promise<UInt16>(resolved: port)
//	})
//}
//
