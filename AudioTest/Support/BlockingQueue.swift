//
//  BlockingQueue.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import Cadmus

class BlockingQueue<Value> {
	let queueLock: NSObject = NSObject()
	let queueSemaphore: DispatchSemaphore = DispatchSemaphore(value: 1)
	
	// Basically we know what's going on here
	var items: [Value] = []
	
	fileprivate var stopped: Bool = false
	
	func start() {
		stop()
		lock(on: queueLock) {
			items.removeAll()
			stopped = false
		}
		log(debug: ">> Started")
	}
	
	func stop() {
		guard !stopped else { return }
		
		lock(on: queueLock) {
			stopped = true
		}
		// Try and drain the queue
		var isEmpty = false
		repeat {
			isEmpty = lock(on: queueLock) {
				return items.isEmpty
			}
			if !isEmpty {
				Thread.sleep(forTimeInterval: 1.0)
			}
		} while !isEmpty
		log(debug: "<< Stopped")
	}
	
	func put(_ value: Value) {
		lock(on: queueLock) {
			guard !stopped else {
				log(warning: "Queue is stopped - not accepting items")
				return
			}
			items.append(value)
//			log(debug: "Queue size: \(items.count)")
			queueSemaphore.signal()
		}
	}
	
	func take() -> Value? {
		var result: Value? = nil
		var acceptable = false
		repeat {
			acceptable = false
			
			result = lock(on: queueLock) {
				guard !items.isEmpty else { return nil }
				return items.removeFirst()
			}
//			log(debug: "Last item = \(result)")
			if result == nil {
				// Have we drained the queue of all items?
				if stopped {
					acceptable = true
				} else {
//					log(debug: "Wait for a item to appear")
					_ = queueSemaphore.wait(timeout: DispatchTime.now() + 3.0)
				}
			} else {
				acceptable = true
			}
		} while !acceptable
//		log(debug: "Popped = \(result)")
		return result
	}
}
