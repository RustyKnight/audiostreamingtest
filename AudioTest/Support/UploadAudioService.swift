//
//  OutgoingAudioServer.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import SwiftSocket
import Cadmus

class UploadAudioService {
	
	let config: DeviceConfiguration
	var socket: TCPClient?

	init(config: DeviceConfiguration) {
		self.config = config
	}
	
	func open() {
		guard socket == nil else { return }
		
		log(info: "Open socket to \(config.ipAddress):\(config.port)")
		socket = TCPClient(address: config.ipAddress, port: Int32(config.port))
		switch socket!.connect(timeout: 30) {
		case .success: sendData()
		case .failure(let error): log(error: "Failed to open socket to \(config.ipAddress): \(error)")
		}
	}
	
	func close() {
		socket?.close()
		socket = nil
	}
	
	fileprivate func sendData() {
		guard let socket = socket else { return }
		defer {
			log(info: "Closing client socket \(socket.address)")
			close()
		}
		
		guard let frameBuffer = try? FrameBuffer() else {
			log(error: "Failed to load frame buffer")
			return
		}
		
		let headerLength = MemoryLayout<SWANN_AUDIO_HEADER_ST>.size
		var count = 0
		
		log(debug: "headerLength = \(headerLength)")
		
		while frameBuffer.hasNext() {
			let nextFrame = frameBuffer.next()
			
			var header = SWANN_AUDIO_HEADER_ST()
			header.start_code = (Int8("S".utf8.first!), Int8("W".utf8.first!), Int8("S".utf8.first!), Int8("W".utf8.first!))
			header.header_length = Int32(headerLength)
			header.encoder_type = 2
			header.sample_rate = 8000
			header.packet_sn = Int32(count)
			header.data_length = Int32(nextFrame.count)
			
			//			log(debug: "header.start_code = \(header.start_code)")
			//			log(debug: "header.header_length = \(header.header_length)")
			//			log(debug: "header.encoder_type = \(header.encoder_type)")
			//			log(debug: "header.sample_rate = \(header.sample_rate)")
			//			log(debug: "header.packet_sn = \(header.packet_sn)")
			//			log(debug: "header.data_length = \(header.data_length)")
			
			var headerData = Data(bytes: &header, count: headerLength)
			headerData.append(nextFrame)
			log(debug: "Seq = \(count); \(nextFrame.count); \(headerData.count)")
			let result = socket.send(data: headerData)
			switch result {
			case .success: break
			case .failure(let error):
				log(error: "\(error)")
				return
			}
			count += 1
			Thread.sleep(forTimeInterval: 0.1)
		}
		Thread.sleep(forTimeInterval: 1.0)
		close()
	}

}
