//
//  TransportService.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import Cadmus
import SwiftSocket

class TransportService {
	
	fileprivate var stopped: Bool = false
	
	var socket: TCPClient?
	let config: DeviceConfiguration
	
	init(config: DeviceConfiguration) {
		self.config = config
	}
	
	func start() throws {
		stop()
		stopped = false
		socket = TCPClient(address: config.ipAddress, port: Int32(config.port))
		log(debug: "Connect to \(config.ipAddress) : \(config.port)")
		switch socket!.connect(timeout: 30) {
		case .success:
			DispatchQueue.global(qos: .userInitiated).async {
				self.transportData()
			}
		case .failure(let error):
			socket = nil
			throw error
		}
	}
	
	func stop() {
		stopped = true
		socket?.close()
		socket = nil
	}
	
	func transportData() {
		guard let socket = socket else { return }

		let headerLength = MemoryLayout<SWANN_AUDIO_HEADER_ST>.size
		var header = SWANN_AUDIO_HEADER_ST()
		header.start_code = (Int8("S".utf8.first!), Int8("W".utf8.first!), Int8("S".utf8.first!), Int8("W".utf8.first!))
		header.header_length = Int32(headerLength)
		header.encoder_type = 2
		header.sample_rate = 8000

		var count = 0

		repeat {
			guard let data = TransportQueue.shared.take() else { return }
			guard data.count > 0 else { return }

			header.packet_sn = Int32(count)
			header.data_length = Int32(data.count)

			var headerData = Data(bytes: &header, count: headerLength)
			headerData.append(data)
			log(debug: "\(count); \(header.data_length); \(headerData.count)")
			let result = socket.send(data: headerData)
			switch result {
			case .success: break
			case .failure(let error):
				log(error: "\(error)")
				return
			}
			count += 1


			Thread.sleep(forTimeInterval: 0.1)
		} while !stopped
		log(debug: "!! Stopped")
	}

//	func transportData() {
//
//		repeat {
//			guard let data = TransportQueue.shared.take() else { return }
//			guard data.count > 0 else { return }
//
//			var remainingBuffer = data
//			var frame = 0
//			log(debug: "Buffer \(data.count)")
//			while let nextFrame = nextFrame(buffer: &remainingBuffer) {
//				frame += 1
//				log(debug: "\(frame): \(nextFrame.count) bytes")
//			}
//
//			Thread.sleep(forTimeInterval: 0.1)
//		} while !stopped
//		log(debug: "!! Stopped")
//	}
//
//	fileprivate func nextFrame(buffer: inout Data) -> Data? {
//		var size: Int = 0
//
//		guard !buffer.isEmpty else { return nil }
//		repeat {
//			if buffer.count < 7 { return nil }
//			if buffer[0] == 0xff && (buffer[1] & 0xf0) == 0xf0 {
//				size |= (Int(buffer[3]) & 0x03) << 11
//				size |= (Int(buffer[4])) << 3
//				size |= (Int(buffer[5]) & 0xe0) >> 5
//				break
//			}
//			//			log(debug: "Skip...")
//			buffer = buffer.advanced(by: 1)
//		} while true
//
//		//		log(debug: "size = \(size); remaing = \(remainingBuffer.count)")
//
//		size = min(size, buffer.count)
//
//		let nextFrame = buffer[0..<size]
//		buffer = buffer.advanced(by: size - 1)
//		return nextFrame
//	}

}
