//
//  ViewController.swift
//  AudioTest
//
//  Created by Shane Whitehead on 13/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import UIKit
import SwiftSocket
import Cadmus
import SkyFloatingLabelTextField
import MediaPlayer
import TUTKKit
import Hydra

class ViewController: UIViewController {
	
	enum AudioState {
		case muted
		case unmuted
	}
	
	@IBOutlet weak var microphoneButton: UIButton!
	
	//	var pullAudioServer: PullAudioService?
	var pushAudioService: PushAudioService?
	
	var microphoneState: AudioState = .muted {
		didSet {
			switch microphoneState {
			case .muted: microphoneButton.setImage(UIImage(named: "MuteMicrophone"), for: [])
			case .unmuted: microphoneButton.setImage(UIImage(named: "Micorphone"), for: [])
			}
		}
	}
	
	var inputTableViewController: InputTableViewController!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Awesome Audio"
		
		microphoneButton.setTitle(nil, for: [])
		microphoneButton.setImage(UIImage(named: "MuteMicrophone"), for: [])
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
	}
	
	// MARK: Segues
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "Segue.config" {
			inputTableViewController = segue.destination as! InputTableViewController
		}
	}
	
	// MARK: Export
	
	func save(config: DeviceConfiguration) -> URL? {
		do {
			let data = try JSONEncoder().encode(config)
			
			let documents = FileManager.default.urls(for: .documentDirectory,
																							 in: .userDomainMask).first
			
			guard let path = documents?.appendingPathComponent("Config.serverCnf") else {
				return nil
			}
			
			try data.write(to: path, options: .atomicWrite)
			return path
		} catch let error {
			log(error: "\(error)")
		}
		return nil
	}
	
	// MARK: Send audio
	
	@IBAction func sendAudio(_ sender: Any) {
		if microphoneState == .muted {
			microphoneState = .unmuted
			
			let config = inputTableViewController.deviceConfiguration
			guard config.ipAddress != "" && config.port != 0 else {
				presentErrorAlertWith(message: "Local is not configured") { (action) in
					self.microphoneState = .muted
				}
				return
			}
			// Upload file test
//			let uploadService = UploadAudioService(config: config)
//			DispatchQueue.global(qos: .userInitiated).async {
//				uploadService.open()
//			}
			
			// Comment this out when using the upload load service
			pushAudioService = PushAudioService()
			do {
				try pushAudioService?.start(config)
			} catch let error {
				log(error: "Failed to spin up push audio service: \(error.localizedDescription)")
				pushAudioService?.stop()
			}
		} else {
			microphoneState = .muted
			pushAudioService?.stop()
			pushAudioService = nil
		}
	}
}
